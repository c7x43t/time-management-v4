import "regenerator-runtime/runtime.js";
import {MyElement} from './index';
var nConditions=2;
var fulfilledConditions=0;
window.addEventListener('WebComponentsReady', function () {fulfilledConditions++;loaded()});
document.addEventListener('DOMContentLoaded',function() {fulfilledConditions++;loaded()});
function loaded(){
    if(nConditions===fulfilledConditions) {
        customElements.define('my-element', MyElement);
    }
}