///

import {LitElement, html, css, customElement, property} from 'lit-element';

// This decorator defines the element.
@customElement('my-element')
export class MyElement extends LitElement {

    // This decorator creates a property accessor that triggers rendering and
    // an observed attribute.
    @property()
    mood = 'great';

    @property()
    daten = 'daten';

    constructor() {
        super();
        var self=this;
        setTimeout(function(){
            self.daten='update';
        },2000);
    }
    static styles = css`
    span:first-child {
        color: green;
        font-size: 20px;
    }`;
    // Render element DOM by returning a `lit-html` template.
    render() {
        return html`Web Components are <span>${this.mood}</span>!
        <span>${this.daten}</span>
        `;
    }
}
/// HMR
if(module){
    // @ts-ignore
    if (module.hot) {
        // @ts-ignore
        module.hot.accept(function(){
            console.log("hmr accept")
        })
        // @ts-ignore
        module.hot.dispose(function() {
            console.log("hmr dispose")
        })
    }
}
//*/
/// global error catching
window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
    console.log("Error occured: " + errorMsg);//or any message
    return false;
}

/// websocket
var exampleSocket = new WebSocket("ws://localhost:3000/test/1");
console.log(exampleSocket)
exampleSocket.onopen = function (event) {
    // exampleSocket.send("Here's some text that the server is urgently awaiting!"); 
}; 
exampleSocket.onmessage = function (event) {
    console.log(event.data);
    // server sends message "rebuild" as soon as parcel finishes building
    if(event.data==="rebuild"){
        location.reload();
    }
}
/// REST
import ky from 'ky';
(async () => {
	//const parsed = await ky.post('/rest', {json: {foo: true}}).json();

	//console.log(parsed);
	//=> `{data: '🦄'}`
})();

// @ts-ignore
window.postData=async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
  }
  
 