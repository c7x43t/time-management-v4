import { createMiddleware } from 'koa-parcel-middleware' // :)
//import { App } from './app' // e.g. a react <App /> component
import { promises as fs } from 'fs'
import * as path from 'path'
//import * as ReactDOMServer from 'react-dom/server'
import Bundler from 'parcel-bundler'
import CombinedStream from 'combined-stream'
import Koa from 'koa'
import route from 'koa-route'
import websockify from 'koa-websocket'
import serveStatic from 'koa-static'
import "reflect-metadata";
import {createConnection} from "typeorm";
import { stringify } from 'querystring'
import { deflateSync } from 'zlib'
const send = require('koa-send');
const koaBody = require('koa-body');
/*
import Router from '@koa/router';
const router = new Router();
router.post('/rest/v1/login2', koaBody(),
  (ctx) => {
    console.log(ctx.request.body);
    // => POST body
    ctx.body = JSON.stringify(ctx.request.body);
  }
);
*/

/*
import Router from '@koa/router';
const router = new Router();
router
  .get('/test', (ctx, next) => {
    ctx.body = 'Hello World!';
  })
*/
// your parcel application's _unbuilt_ entry point!
const ENTRY_FILENAME = path.resolve(__dirname, '../client/index.html')
// Production mode:
// https://github.com/parcel-bundler/parcel/issues/355
const isDev = true //process.env.NODE_ENV === 'development'
const PORT = 3000; 

let map = new Map();
async function start () {
  const app = websockify(new Koa())
  app.use(koaBody());
  app.ws.use(route.all('/test/:id', function (ctx) {
    // `ctx` is the regular koa context created from the `ws` onConnection `socket.upgradeReq` object.
    // the websocket is added to the context on `ctx.websocket`.
    map.set("ws",ctx.websocket);
    //ctx.websocket.send('Hello World');
    ctx.websocket.on('message', function(message) {
      // do something with the message from client
          console.log(message);
          ctx.websocket.send(message);
    });
  }));
  // your parcel application's _built_ entry point!
  const outFile = path.resolve(__dirname, '../client/dist', 'index.html')
  const outDir = path.resolve(__dirname, '../client/dist')
  const options = {
    outDir,
    outFile,
    watch: isDev,
    publicUrl: '/',
    minify: true,// !isDev,
    scopeHoist: false,
    hmr: isDev,
    detailedReport: isDev
  }
  const bundler = new Bundler(ENTRY_FILENAME, options)
  bundler.on('buildEnd', () => {
    // send page reload request
    // console.log("Finished building");
    if(map.has("ws")){
      map.get("ws").send("rebuild")
    }
  });
  bundler.bundle()
  const staticMiddleware = serveStatic(outDir)
  const parcelMiddleware = createMiddleware({
    bundler,
    renderHtmlMiddleware: async (ctx, next) => {
      // optionally wire in SSR!
 
      // index.html
      //
      // <html>
      //   <div id="app"><!-- ssr-content --></div>
      //   <script src="app.tsx"></script>
      // </html>
      const outFileBuffer = await fs.readFile(outFile)
      const [preAppEntry, postAppEntry] = outFileBuffer
        .toString()
        .split(/<!--.*ssr.*-->/)
      ctx.status = 200
      const htmlStream = new CombinedStream()
      /*;[
        preAppEntry,
        //ReactDOMServer.renderToNodeStream(App()),
        postAppEntry
      ].map(content => htmlStream.append(content))*/
      htmlStream.append(outFileBuffer.toString())
      ctx.body = htmlStream
      ctx.type = 'html'
      await next()
    },
    staticMiddleware 
  })
  // parcel middleware
  app.use((ctx, next) => parcelMiddleware(ctx, next))
  // setting CORS policy
  app.use(async (ctx, next) => {
    ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    await next();
  });
  // setting Timing API policy
  app.use(async (ctx, next) => {
    ctx.set('Timing-Allow-Origin', '*');
    await next();
  });

  //randomIdGenerator
  function makeid(length: number) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

  //app.use(router.routes())
  //.use(router.allowedMethods());
  app.use(route.get('/', async(ctx, res) => {
    await send(ctx, '/src/client/websites/login.html');
  }));
  
  var datenbank={sessions:new Map()};

  app.use(route.post('/rest/v1/login', async(ctx, next) => {
    console.log('Request data: ' + JSON.stringify(ctx.request.body));
    let body = ctx.request.body;
    var user=data.users.get(body.email);
    var success=user && body.password === user.password;
    var obj={success:success,session:"",roles:user.roles,name:user.name};
    if(success){
      var sessionKey = makeid(31);
      obj.session=sessionKey;
      datenbank.sessions.set(sessionKey,true);
    }
    ctx.body = JSON.stringify(obj);
    next();
    console.log('Response: ' + JSON.stringify(obj));
  })); 
  var dataTable=[];
  var date=new Date();
  var day=date.getDate();
  var month=date.getMonth()+1;
  var year=date.getFullYear();
  //var days=[];
  var currentDate;
  for(var i=1;i<=31;i++){
    currentDate=new Date(`${month}.${i}.${year}`);
    var dayName;
    if(currentDate){
      // @ts-ignore: Object is possibly 'null'.
      dayName= /(\w+),/.exec(currentDate.toUTCString() as string)[1];
    } 
    var _data = {
      day: `${(i+"").padStart(2,"0")}.${month}.${year}`,
      weekay: dayName,
      begin: 8,
      end: 17,
      break: 0.75,
      hoursTarget:8,
      hoursAsIs:0,
      delta:0,
      absence: 0,
      comment: ""
    }
    dataTable.push(_data);
  }
  
  
  

  var data = {
    table:dataTable,
    users:new Map()
  };
  // create Mock users
  var users=[{
    name:"Max Mustermann",
    email:"max@mail.de",
    password:"admin",
    roles:["Employee","Supervisor","HR"]
  },
  {
    name:"Anna Anemarie",
    email:"anna@mail.de",
    password:"admin",
    roles:["Employee"]
  },
  {
    name:"Leon Luchs",
    email:"leon@mail.de",
    password:"admin",
    roles:["Employee","Supervisor"]
  }
]
  users.forEach(user=>data.users.set(user.email,user));

  app.use(route.post('/rest/v1/getTimesheet',function(ctx){
    let body = ctx.request.body;
    body.timeSheetId;
    // logic
    ctx.body = JSON.stringify(data.table);
  }));
  app.use(route.post('/rest/v1/setTimesheet',function(ctx){
    let body = ctx.request.body;
    console.log(body);
    data.table=body;
    // logic
    ctx.body = JSON.stringify(data.table);
  }));

  app.use(route.get('/home', async(ctx, res) => {
    console.log(ctx.cookies.get('session'));
    var session=ctx.cookies.get('session');
    if(session!==undefined&&datenbank.sessions.has(session)){
      await send(ctx, '/src/client/websites/home.html');
    }else{
      await send(ctx, '/src/client/websites/login.html');
    }
  }));
  app.use(route.get('/timesheet', async(ctx, res) => {
    console.log(ctx.cookies.get('session'));
    var session=ctx.cookies.get('session');
    if(session!==undefined&&datenbank.sessions.has(session)){
      await send(ctx, '/src/client/websites/list.html');
    }else{
      await send(ctx, '/src/client/websites/login.html');
    }
  }));
  app.use(route.get('/vacation', async(ctx, res) => {
    console.log(ctx.cookies.get('session'));
    var session=ctx.cookies.get('session');
    if(session!==undefined&&datenbank.sessions.has(session)){
      await send(ctx, '/src/client/websites/vacation.html');
    }else{
      await send(ctx, '/src/client/websites/login.html');
    }
  }));
  app.use(route.post('/rest/v1/register', (ctx, next) => {
    // console.log(ctx.body);
    console.log(ctx.request.body);
    console.log(ctx.params);
    let body = ctx.request.body;
    body.newData = 'server added data';
    var obj={success:true,session:'abcde'}
    ctx.body = JSON.stringify(obj);
    next();
  }));
  // app.use(router.routes());
  app.listen(PORT)
  console.log("Listening on port: ", PORT)

}
start()